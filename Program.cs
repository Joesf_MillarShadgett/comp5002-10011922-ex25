﻿using System;
using System.Collections.Generic;

namespace Exercises
{
    // For Part A
    public class myClass
    {
        // Don't need do specify private access as things are implicitly private.
        double field01, field02;

        public myClass(double _field01, double _field02)
        {
            field01 = _field01;
            field02 = _field02;
        }

        public void PrintFields()
        {
            Console.WriteLine($"Field 1 = {field01}, Field 2 = {field02}.\n");
        }
    }

    // For part B
    public class Checkout
    {
        int quantity = 0;
        double price = 0.0;

        public Checkout(int _quantity, double _price)
        {
            quantity = _quantity;
            price = _price;
        }

        public double GetPrice()
        {
            return price * quantity;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();

            // Part A
            myClass myC = new myClass(1.5, 2.2);
            myC.PrintFields();
            
            // Part B
            Console.WriteLine("\nPart B\n");

            double total = 0.0;

            List<Checkout> c = new List<Checkout>();
            c.Add(new Checkout(5, 1.90));
            c.Add(new Checkout(2, 9.90));
            c.Add(new Checkout(20, 5.50));

            for (int i = 0; i < c.Count; i++)
            {
                total += c[i].GetPrice();
            }
            Console.WriteLine($"Total = {total}");

            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}
